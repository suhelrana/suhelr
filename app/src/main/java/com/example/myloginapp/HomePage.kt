package com.example.myloginapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.FirebaseAuth
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_home_page.*
import kotlinx.android.synthetic.main.item_place.view.*

class HomePage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)

        verifyLogIn()

        val adapter = GroupAdapter<ViewHolder>()

        val hotel1 = Hotel("Pan Pacific Sonargaon", "5 Star", "$187 each Night")
        val hotel2 = Hotel("Radisson Blu", "5 Star", "$230 each Night")
        val hotel3 = Hotel("Orchard Suites", "4 Star", "$60 each Night")
        val hotel4 = Hotel("City Homes", "3 Star", "$53 each Night")
        val hotel5 = Hotel("Hotel Sweet Dream", "3 Star", "$65 each Night")

        adapter.add(ItemPlace(hotel1))
        adapter.add(ItemPlace(hotel2))
        adapter.add(ItemPlace(hotel3))
        adapter.add(ItemPlace(hotel4))
        adapter.add(ItemPlace(hotel5))

        rvAdpater.adapter = adapter

        val dividerItemDecoration = DividerItemDecoration(this, 1)

        rvAdpater.addItemDecoration(dividerItemDecoration)

        adapter.setOnItemClickListener { item, view ->
            val myItem = item as ItemPlace
            Toast.makeText(view.context, "${myItem.hotel.name}", Toast.LENGTH_SHORT).show()
        }
    }

    private fun verifyLogIn() {
        val userId = FirebaseAuth.getInstance().uid

        if (userId == null){
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.signOut -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}

class ItemPlace(val hotel: Hotel): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        Log.d("bind", "found bind")
        viewHolder.itemView.tvPlaceName.text = hotel.name
        viewHolder.itemView.tvPlaceRating.text = hotel.rating
        viewHolder.itemView.tvPlaceDescription.text = hotel.description
    }

    override fun getLayout(): Int {
        return R.layout.item_place
    }
}

class Hotel(val name: String, val rating: String, val description: String)
