package com.example.myloginapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvSignUpPage.setOnClickListener {
            val intent = Intent(this, SignUp::class.java)
            startActivity(intent)
        }

        btnSignIn.setOnClickListener {
            logdIn()
        }
    }

    private fun logdIn(){
        val signInPassword = etSignInPassword.text.toString().trim()
        val signInEmail = etSignInEmail.text.toString().trim()

        if (signInPassword.isEmpty() && signInEmail.isEmpty()){
            Toast.makeText(this, "Enter email and pass", Toast.LENGTH_LONG).show()
            return
        }
        showProgressbar()

        FirebaseAuth.getInstance().signInWithEmailAndPassword(signInEmail, signInPassword)
            .addOnCompleteListener {
                if(!it.isSuccessful){
                    hideProgressbar()
                    return@addOnCompleteListener
                }
                val intent = Intent(this, HomePage::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                hideProgressbar()
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                hideProgressbar()
            }
    }
    private fun showProgressbar(){
        ivImage.visibility = View.GONE
        etSignInEmail.visibility = View.GONE
        etSignInPassword.visibility = View.GONE
        btnSignIn.visibility = View.GONE
        tvSignUpPage.visibility = View.GONE
        pbSignInProgress.visibility = View.VISIBLE
        tvSignInProgressText.visibility = View.VISIBLE
    }

    private fun hideProgressbar(){
        ivImage.visibility = View.VISIBLE
        etSignInEmail.visibility = View.VISIBLE
        etSignInPassword.visibility = View.VISIBLE
        btnSignIn.visibility = View.VISIBLE
        tvSignUpPage.visibility = View.VISIBLE
        pbSignInProgress.visibility = View.GONE
        tvSignInProgressText.visibility = View.GONE
    }
}
