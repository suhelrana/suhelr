package com.example.myloginapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_sign_up.*

@Suppress("UNUSED_VALUE")
class SignUp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        tvSignInPage.setOnClickListener {
            finish()
        }
        btnSignUp.setOnClickListener {
            saveData()
        }
    }

    private fun saveData(){
        val signUpName = etSignUpName.text.toString().trim()
        val signUpEmail = etSignUpEmail.text.toString().trim()
        val signUpPassword = etSignUpPassword.text.toString().trim()
        val signUpUserName = etSignUpUserName.text.toString().trim()

        if(signUpName.isEmpty() && signUpEmail.isEmpty() && signUpPassword.isEmpty() && signUpUserName.isEmpty()){
            Toast.makeText(this, "Enter all the Information!!", Toast.LENGTH_LONG).show()
            return
        }
        showProgressbar()
        var checkPasswordSpacialCharacter = false
        var checkPasswordCharacter = false
        var checkPasswordLetter = false
        for(i in signUpPassword){
            if(i == '@' || i == '#'|| i == '$' || i == '&' || i == '*' || i == '.') {
                checkPasswordSpacialCharacter = true
                if(checkPasswordCharacter && checkPasswordLetter) break
            }
            if(i in 'a'..'z' || i in 'A'..'Z'){
                checkPasswordCharacter = true
                if(checkPasswordSpacialCharacter && checkPasswordLetter) break
            }
            if(i in '0'..'9'){
                checkPasswordLetter = true
                if(checkPasswordCharacter && checkPasswordSpacialCharacter) break
            }
        }
        if(!checkPasswordSpacialCharacter || ! checkPasswordCharacter || !checkPasswordLetter){
            hideProgressbar()
            tvPass.text = "(your password must be the combination of characters, letters and at least One spacial character like @ # $ & * .)"
            return
        }

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(signUpEmail, signUpPassword)
            .addOnCompleteListener {
                if(!it.isSuccessful) return@addOnCompleteListener

                val uid = FirebaseAuth.getInstance().uid?: ""
                val ref = FirebaseDatabase.getInstance().getReference("/users/$signUpName($uid)")

                val user = Users(uid, signUpName, signUpEmail, signUpPassword, signUpUserName)
                ref.setValue(user)
                    .addOnSuccessListener {
                        hideProgressbar()
                        val intent = Intent(this, HomePage::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                        hideProgressbar()
                        tvPass.visibility = View.GONE
                    }
            }
            .addOnFailureListener {
                Toast.makeText(this, "${it.message}", Toast.LENGTH_LONG).show()
                hideProgressbar()
                tvPass.visibility = View.GONE
            }
    }

    private fun showProgressbar(){
        etSignUpName.visibility = View.GONE
        etSignUpEmail.visibility = View.GONE
        etSignUpPassword.visibility = View.GONE
        etSignUpUserName.visibility = View.GONE
        btnSignUp.visibility = View.GONE
        tvPass.visibility = View.GONE
        tvProgressText.visibility = View.VISIBLE
        pbProgress.visibility = View.VISIBLE
    }

    private fun hideProgressbar(){
        etSignUpName.visibility = View.VISIBLE
        etSignUpEmail.visibility = View.VISIBLE
        etSignUpPassword.visibility = View.VISIBLE
        etSignUpUserName.visibility = View.VISIBLE
        btnSignUp.visibility = View.VISIBLE
        tvPass.visibility = View.VISIBLE
        tvProgressText.visibility = View.GONE
        pbProgress.visibility = View.GONE
    }
}
class Users(val uid: String, val name: String, val email: String, val password: String, val userName: String)
